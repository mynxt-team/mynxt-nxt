module.exports = function (grunt) {
  var jsSrc = [
    'html/ui/js/3rdparty/jquery.js',
    'html/ui/js/3rdparty/bootstrap.js',
    'html/ui/js/3rdparty/i18next.js',
    'html/ui/js/3rdparty/ajaxretry.js',
    'html/ui/js/3rdparty/ajaxmultiqueue.js',
    'html/ui/js/3rdparty/jquery.rss.js',
    'html/ui/js/3rdparty/webdb.js',
    'html/ui/js/3rdparty/growl.js',
    'html/ui/js/3rdparty/zeroclipboard.js',
    'html/ui/js/3rdparty/jsbn.js',
    'html/ui/js/3rdparty/jsbn2.js',
    'html/ui/js/3rdparty/big.js',
    'html/ui/js/3rdparty/pako.js',
    'html/ui/js/3rdparty/maskedinput.js',
    'html/ui/js/3rdparty/qrcode.js',
    'html/ui/js/3rdparty/handlebars.js',
    'html/ui/js/util/extensions.js',
    'html/ui/js/util/converters.js',
    'html/ui/js/util/nxtaddress.js',
    'html/ui/js/crypto/3rdparty/jssha256.js',
    'html/ui/js/crypto/curve25519.js',
    'html/ui/js/crypto/curve25519_.js',
    'html/ui/js/crypto/3rdparty/cryptojs/aes.js',
    'html/ui/js/crypto/3rdparty/cryptojs/sha256.js',
    'html/ui/js/nrs.sitebuild.js',
    'html/ui/js/nrs.js',
    'html/ui/js/nrs.server.js',
    'html/ui/js/nrs.forms.js',
    'html/ui/js/nrs.login.js',
    'html/ui/js/nrs.update.js',
    'html/ui/js/nrs.recipient.js',
    'html/ui/js/nrs.assetexchange.js',
    'html/ui/js/nrs.monetarysystem.js',
    'html/ui/js/nrs.dgs.js',
    'html/ui/js/nrs.messages.js',
    'html/ui/js/nrs.aliases.js',
    'html/ui/js/nrs.polls.js',
    'html/ui/js/nrs.blocks.js',
    'html/ui/js/nrs.transactions.js',
    'html/ui/js/nrs.peers.js',
    'html/ui/js/nrs.contacts.js',
    'html/ui/js/nrs.news.js',
    'html/ui/js/nrs.settings.js',
    'html/ui/js/nrs.sidebar.js',
    'html/ui/js/nrs.encryption.js',
    'html/ui/js/nrs.modals.js',
    'html/ui/js/nrs.modals.account.js',
    'html/ui/js/nrs.modals.accountdetails.js',
    'html/ui/js/nrs.modals.block.js',
    'html/ui/js/nrs.modals.forging.js',
    'html/ui/js/nrs.modals.info.js',
    'html/ui/js/nrs.modals.token.js',
    'html/ui/js/nrs.modals.transaction.js',
    'html/ui/js/nrs.modals.accountinfo.js',
    'html/ui/js/nrs.modals.balanceleasing.js',
    'html/ui/js/nrs.modals.advanced.js',
    'html/ui/js/nrs.modals.dividendpayment.js',
    'html/ui/js/nrs.console.js',
    'html/ui/js/nrs.util.js',
    'html/ui/js/*.js',
    '!html/ui/js/ats.js',
    '!html/ui/js/ats.util.js',
  ];

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      options: {
        // define a string to put between each file in the concatenated output
        separator: ';\n',
        stripBanners: {
          block: true,
          line: true
        }
      },
      dist: {
        // the files to concatenate
        src: jsSrc,
        // the location of the resulting JS file
        dest: 'html/ui/dist/nrs.js'
      }
    },
    uglify: {
      options: {
        // the banner is inserted at the top of the output
        banner: '/*! nrs <%= grunt.template.today("dd-mm-yyyy") %> */\n',
        mangle: false
      },
      dist: {
        files: {
          'html/ui/dist/nrs.js': ['html/ui/dist/nrs.js']
        }
      }
    },
    watch: {
      scripts: {
        files: jsSrc,
        tasks: ['concat'],
        options: {
          spawn: false
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');

  grunt.registerTask('default', [
    'concat',
    'watch'
  ]);
  grunt.registerTask('build', [
    'concat',
    'uglify'
  ]);
};